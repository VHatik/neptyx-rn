import React from "react";
import styled from "styled-components/native";


export default function Button ( { color, width, height, border, background, text, icon, borderColor } ) {

    return (
        <MainButton width={width} height={height} border={border} background={background} borderColor={borderColor} >
            <TextButton color={color}>{text}</TextButton>
            {icon && <IconButton source={require('../../../../assets/image/utils/vk_icon.png')}/>}
        </MainButton>
)}

Button.defaultProps = {
    text: 'Untitled'
}

const MainButton = styled.TouchableOpacity`
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: row;
    border-radius: ${props => props.border || '5px'}; 
    width:  ${props => props.width || '160px'};
    height:  ${props => props.height || '46px'};
    background: ${props => props.background || 'white'};
    border: 0.5px solid ${props => props.borderColor || '#403F47'};;
`

const TextButton = styled.Text`
        color:  ${props => props.color || 'black'};
        font-size: 17px;
`
const IconButton = styled.Image`
    margin-top: 2px;
    margin-left: 20px;
`
