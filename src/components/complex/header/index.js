import React from "react";
import styled from "styled-components/native";
import Container from "../../layouts/Container"


export default function CustomHeader( ) {
    return (
        <Header>
            <Container>
                <IconHeader
                    source = {require('../../../../assets/image/utils/logo_header.png')}/>
                    <CountHeader>
                        💎 300
                    </CountHeader>
            </Container>
        </Header>
    );
}

const Header = styled.View`
    justify-content: start;
    height: 80px;
    background: red;
`
const IconHeader = styled.Image`
    width: 100px;
    height: 19px;
`
const CountHeader = styled.Text`
    margin-top: 7px;
    color: white;
`