import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer, DefaultTheme } from '@react-navigation/native';

import {Preloader, Home, Intro,} from "../screens";


const Stack = createStackNavigator();

const MyTheme = {
    ...DefaultTheme,
    colors: {
        ...DefaultTheme.colors,
        primary: 'rgb(0,64,226)',
        background: '#030303',
        card: '#030303',
        text: 'rgba(255,255,255,1)',
        border: '#030303',
        notification: '#ffffff'
    },
    dark: false
};

const headerStyle = { 
    color: 'white'
}

const StackNavigation = () => (
    <NavigationContainer theme={MyTheme}>
        <Stack.Navigator screenOptions={{
            headerShown: false
        }}>
            <Stack.Screen name="Home" component={Home} options={{}}
            />
            
        </Stack.Navigator>
    </NavigationContainer>
)

export default StackNavigation;


