import React from "react";
import {StatusBar} from "expo-status-bar";
import styled from "styled-components/native";
import CustomHeader from "../components/complex/header"
import Container from "../components/layouts/Container"


export default function Home( ) {
    return (
    
        <Background
            source = {require('../../assets/image/bg/bg_home.png')}
        >
        <StatusBar style="auto" />
            <CustomHeader/>
            <View1/>
        </Background>
    
    );
}

Home.defaultProps = {
    title: 'Главная'
}
const Background = styled.ImageBackground`
    width: 100%;
    height: 100%;
`
const View1 = styled.View`
    flex: 2;
    background: green;
`



