import { Text } from 'react-native';
import React from "react";
import styled from "styled-components/native";
import LogoWrapper from '../components/layouts/LogoWrapper.js';
import Button from '../components/common/Button.js';




export default function Preloader( ) {
    
    // return (
    //     <Background source = {require('../../assets/image/bg/bg2.png')}>
    //         <TopWrapper>
    //             <LogoWrapper width='36px' height='43px'/>
    //             <ButtonWrapper>
    //                 <Button background='transparent' color='#403F47' text='Пропустить' height='40px'/>
    //             </ButtonWrapper>
    //         </TopWrapper>
    //         <CenterWrapper>
    //             <ImageWrapper>
    //                 <Image source={require('../../assets/image/utils/art_ico_screen2.png')}/>
    //             </ImageWrapper>
    //         </CenterWrapper>
    //         <BottomWrapper>
    //             <SwipeWrapper>
    //                     <SwipeText>
    //                         Свайпни вниз
    //                     </SwipeText>
    //                     <SwipeIcon source={require('../../assets/image/utils/Swipe_ico.png')}/>
    //             </SwipeWrapper>
    //             <InfoTextWrapper>
    //                 <InfoText>
    //                     Рассылайте биты{"\n"}исполнителям
    //                 </InfoText>
    //             </InfoTextWrapper>
    //         </BottomWrapper> 
            
    //     </Background>
    // )}

    // return (
    //     <Background source = {require('../../assets/image/bg/bg3.png')}>
    //         <TopWrapper>
    //             <LogoWrapper width='36px' height='43px'/>
    //         </TopWrapper>
    //         <CenterWrapper>
    //             <ImageWrapper>
    //                 <Image width='263px' height='285px' source={require('../../assets/image/utils/art_ico_screen3.png')}/>
    //             </ImageWrapper>
    //         </CenterWrapper>
    //         <BottomWrapper>
    //             <InfoTextWrapper>
    //                 <InfoText>
    //                     Большая база{"\n"}артистов
    //                 </InfoText>
    //             </InfoTextWrapper>
    //         </BottomWrapper> 
            
    //     </Background>
    // )}

    return (
            <Background source = {require('../../assets/image/bg/bg4.png')}>
                <TopWrapper>
                    <LogoWrapper width='36px' height='43px'/>
                </TopWrapper>
                <CenterWrapper>
                    <ImageWrapper>
                        <Image width='225px' height='306px' source={require('../../assets/image/utils/art4.png')}/>
                    </ImageWrapper>
                </CenterWrapper>
                <BottomWrapper>
                    <InfoTextWrapper>
                        <InfoText>
                            Полностью{"\n"}бесплатное приложение
                        </InfoText>
                    </InfoTextWrapper>
                </BottomWrapper>  
            </Background>
        )}

const Background = styled.ImageBackground`
    width: 100%;
    height: 100%;
`
const TopWrapper = styled.View`
    flex: 2;
`
const CenterWrapper = styled.View`
    flex: 3;
`
const BottomWrapper = styled.View`
    flex: 2;
`

const ButtonWrapper = styled.View`
    align-items: center;
    justify-content: center;
`
const ImageWrapper = styled.View`
    align-items: center;
    justify-content: center;
 
`
const Image = styled.Image`
    width: ${props => props.width || '292.51px'}; 
    height: ${props => props.height || '302px'}; 
`
const InfoTextWrapper = styled.View`
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;
    padding: 50px 20px;
`
const InfoText = styled.Text`
    text-align: center;
    font-weight: bold;
    font-size: 28px;
    color: white;
`
const SwipeWrapper = styled.View`
    align-items: center;
    justify-content: center;
    padding: 10px;
`
const SwipeText = styled.Text`
    font-weight: 500;
    font-size: 18px;
    color: white;
`
const SwipeIcon = styled.Image`
   margin-top: 15px;
`


